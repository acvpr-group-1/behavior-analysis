# Behavior Analysis

This repository contains the code for the driver behavior classification project
using the AIDE dataset. We will fine-tune a Vision Transformer model to classify
driver behavior according to the in-car images and the driver's emotion.

We will also use explanaible AI techniques to explain said model.


## The Code

Script used for training the ViT: `ViTDD/train_vit_aide.py`.
Script used for training the ViT-DD: `ViTDD/train_vit_dd_aide.py`.
Script used for testing: `ViTDD/test_vit.py`.

Notebooks are used for implementing explainable AI.

The `samples` directory has samples of each category.
The `face_samples` directory has the cut-out faces of each driver in each category.
The `report` directory contains images and codes used for the report.


The `ViTDD` folder contains the repository of the implementation of the ViT-DD  (Yunsheng Ma and Ziran Wang, https://github.com/PurdueDigitalTwin/ViT-DD). 
It also contains code to train and test the ViT-DD.
It also has code for some of the explainable AI techniques.

Note that the AIDE data is not stored in this repository and is therefore not accessible.