import torch
from torch.utils.data import DataLoader, RandomSampler, SequentialSampler
from pytorch_lightning import LightningDataModule
from pytorch_lightning import Trainer
from train import ViTDDLM, VisionTransformerLM
from timm.data.parsers.parser import Parser
import timm 
from pathlib import Path
from timm.data import ImageDataset
from pytorch_lightning.utilities.types import TRAIN_DATALOADERS, EVAL_DATALOADERS
from torchvision.transforms import InterpolationMode, GaussianBlur, RandomSolarize, RandomGrayscale
from torchvision import transforms
from timm.data.constants import IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD
import csv
import os
from PIL import Image
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.loggers import TensorBoardLogger

'''
conda create -n acvpr "python<3.9" -y
conda activate acvpr
conda install -y pytorch==1.13.1 torchvision==0.14.1 torchaudio==0.13.1 pytorch-cuda=11.6 -c pytorch -c nvidia
conda install -y timm lightning pathlib matplotlib seaborn ipykernel tensorboard
python -m ipykernel install --prefix /home/jovyan/.local --name acvpr
'''

class AIDEParser(Parser):
    def __init__(self, root, img_list, split):
        super(AIDEParser, self).__init__()

        self.root = Path(root)
        self.subjects = []
        self.classes = []
        self.samples = []

        with open(img_list, 'r') as f:
            csvreader = csv.reader(f)
            fields = next(csvreader)
            for row in csvreader:
                subject, cls, img = row
                self.subjects.append(subject)
                self.classes.append(cls)
                img_path = self.root / split / cls / img
                self.samples.append(img_path)


        classes = sorted(set(self.classes), key=lambda s: s.lower())
        self.class_to_idx = {c: idx for idx, c in enumerate(classes)}

    def __getitem__(self, index):
        path = self.samples[index]
        target = self.class_to_idx[self.classes[index]]
        return open(path, 'rb'), target

    def __len__(self):
        return len(self.samples)

    def _filename(self, index, basename=False, absolute=False):
        filename = self.samples[index]  # absolute path

        if basename:
            filename = filename.parts[-1]
        elif not absolute:
            filename = filename.relative_to(self.root)

        return filename


class AIDEMTLParser(AIDEParser):
    def __init__(self, root, img_list, split, emo_path):
        super(AIDEMTLParser, self).__init__(root, img_list, split)

        emo_list = str(emo_path / "emo_list.csv")
        emo_label_dict = {}
        with open(emo_list, 'r') as f:
            csvreader = csv.reader(f)
            for row in csvreader:
                img_rel_path, emo_label = row
                img_id = img_rel_path.split('/')[-1]
                emo_label_dict[img_id] = int(emo_label)

        self.emo_labels = []
        self.face_images = []
        for path in self.samples:
            path = str(path)
            img_id = path.split(os.sep)[-1]
            emo_label = emo_label_dict[img_id]
            self.emo_labels.append(emo_label)
            if emo_label == -1:
                self.face_images.append("")
            else:
                img_id = img_id.split('.')[0]
                face_img_path = emo_path / 'imgs' / f"{img_id}_face.jpg"
                assert face_img_path.exists(), "face image does not exist"
                self.face_images.append(face_img_path)

    def __getitem__(self, index):
        face_img_path = self.face_images[index]
        emo_target = self.emo_labels[index]
        face_img = None
        if face_img_path:
            face_img = open(face_img_path, 'rb')

        return *super(AIDEMTLParser, self).__getitem__(index), face_img, emo_target


class DistractedDriverLDM(LightningDataModule):
    def __init__(self,
                 batch_size: int = 256,
                 num_workers: int = 4,
                 dataset: str = "AIDE",
                 data_root: str = "./datasets",
                 annotation_path: str = "./annotations",
                 train_list: str = "train_list_01.csv",
                 val_list: str = "validation_list_01.csv",
                 pred_list: str = "test_list_01.csv",
                 input_size: int = 224,
                 color_jitter: float = 0.3,
                 three_augment: bool = True,
                 src: bool = False,  # simple random crop
                 ):
        super(DistractedDriverLDM, self).__init__()
        self.save_hyperparameters()
        self.data_path = Path(self.hparams.data_root) / self.hparams.dataset
        if self.hparams.dataset == "AUCDD":
            self.data_path = self.data_path / "v2" / "cam1"
        self.annotation_path = Path(self.hparams.annotation_path) / self.hparams.dataset
        self.parser_class = eval(f"{self.hparams.dataset}Parser")
        self.train_transforms = self.build_transform(is_train=True)
        self.eval_transforms = self.build_transform(is_train=False)

    def build_transform(self, is_train, input_size: int = 0):
        img_size = self.hparams.input_size if not input_size else input_size
        interpolation = InterpolationMode.BICUBIC
        remove_random_resized_crop = self.hparams.src
        color_jitter = self.hparams.color_jitter

#         if is_train:
#             if remove_random_resized_crop:
#                 primary_tfl = [
#                     transforms.Resize(img_size, interpolation=interpolation),
#                     transforms.RandomCrop(img_size, padding=4, padding_mode='reflect'),
#                     transforms.RandomHorizontalFlip()
#                 ]
#             else:
#                 primary_tfl = [
#                     timm.data.transforms.RandomResizedCropAndInterpolation(img_size, interpolation='bicubic'),
#                     transforms.RandomHorizontalFlip()
#                 ]
#             secondary_tfl = []
#             if self.hparams.three_augment:
#                 secondary_tfl.append(
#                     transforms.RandomChoice([RandomGrayscale(p=1.0), RandomSolarize(threshold=0, p=1.0), GaussianBlur(kernel_size=(5,5))]))
#             if color_jitter is not None and not color_jitter == 0:
#                 secondary_tfl.append(transforms.ColorJitter(color_jitter, color_jitter, color_jitter))
#             final_tfl = [
#                 transforms.ToTensor(),
#                 transforms.Normalize(mean=IMAGENET_DEFAULT_MEAN, std=IMAGENET_DEFAULT_STD)
#             ]
#             return transforms.Compose(primary_tfl + secondary_tfl + final_tfl)

#         else:
        t = []
        t.append(transforms.Resize(img_size, interpolation=InterpolationMode.BICUBIC))
        t.append(transforms.CenterCrop(img_size))
        t.append(transforms.ToTensor())
        t.append(transforms.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD))
        return transforms.Compose(t)

    def train_dataloader(self) -> TRAIN_DATALOADERS:
        train_list = str(self.annotation_path / self.hparams.train_list)
        train_dataset = ImageDataset(str(self.data_path),
                                     parser=self.parser_class(self.data_path, train_list, "train"),
                                     transform=self.train_transforms)
        train_sampler = RandomSampler(train_dataset)
        return DataLoader(train_dataset,
                          sampler=train_sampler,
                          batch_size=self.hparams.batch_size,
                          num_workers=self.hparams.num_workers,
                          pin_memory=True,
                          persistent_workers=True,
                          drop_last=True)

    def val_dataloader(self) -> EVAL_DATALOADERS:
        val_list = str(self.annotation_path / self.hparams.val_list)
        val_dataset = ImageDataset(str(self.data_path),
                                   parser=self.parser_class(self.data_path, val_list, "validation"),
                                   transform=self.eval_transforms)
        val_sampler = SequentialSampler(val_dataset)
        return DataLoader(val_dataset,
                          sampler=val_sampler,
                          batch_size=int(1.5 * self.hparams.batch_size),
                          num_workers=self.hparams.num_workers,
                          pin_memory=True,
                          persistent_workers=True,
                          drop_last=False)

    def predict_dataloader(self) -> EVAL_DATALOADERS:
        pred_list = str(self.annotation_path / self.hparams.pred_list)
        pred_dataset = ImageDataset(str(self.data_path),
                                    parser=self.parser_class(self.data_path, pred_list, "test"),
                                    transform=self.eval_transforms)
        return DataLoader(pred_dataset,
                          sampler=RandomSampler(pred_dataset),
                          batch_size=int(1.5 * self.hparams.batch_size),
                          num_workers=self.hparams.num_workers,
                          pin_memory=True,
                          persistent_workers=True,
                          drop_last=False)

    def test_dataloader(self) -> EVAL_DATALOADERS:
        return self.val_dataloader()


class MTLDistractedDriverLDM(DistractedDriverLDM):
    def __init__(self,
                 batch_size: int = 256,
                 num_workers: int = 4,
                 dataset: str = "AIDE",
                 data_root: str = "./datasets",
                 annotation_path: str = "./annotations",
                 pseudo_label_path: str = "./pseudo_emo_label",
                 train_list: str = "train_list_01.csv",
                 val_list: str = "validation_list_01.csv",
                 pred_list: str = "test_list_01.csv",
                 input_size_1: int = 224,
                 input_size_2: int = 32,
                 color_jitter: float = 0.3,
                 three_augment: bool = True,
                 src: bool = True,
                 ):
        super(MTLDistractedDriverLDM, self).__init__(batch_size, num_workers, dataset, data_root, annotation_path,
                                                     train_list, val_list, pred_list, input_size_1, color_jitter,
                                                     three_augment, src)
        self.input_size_1, self.input_size_2 = input_size_1, input_size_2
        self.train_transforms_1, self.eval_transforms_1 = self.train_transforms, self.eval_transforms
        self.train_transforms_2 = self.build_transform(is_train=True, input_size=input_size_2)
        self.eval_transforms_2 = self.build_transform(is_train=False, input_size=input_size_2)
        self.pseudo_label_path = Path(pseudo_label_path)
        self.parser_class = eval(f"{self.hparams.dataset}MTLParser")

    def train_dataloader(self) -> TRAIN_DATALOADERS:
        train_list = str(self.annotation_path / self.hparams.train_list)
        parser = self.parser_class(root=self.data_path, img_list=train_list, split="train", emo_path=self.pseudo_label_path / self.hparams.dataset)
        train_dataset = MTLDataset(self.data_path, parser,
                                   transform=[self.train_transforms, self.train_transforms_2],
                                   input_sizes=[self.hparams.input_size_1, self.hparams.input_size_2])
        train_sampler = RandomSampler(train_dataset)
        return DataLoader(train_dataset,
                          sampler=train_sampler,
                          batch_size=self.hparams.batch_size,
                          num_workers=self.hparams.num_workers,
                          pin_memory=True,
                          persistent_workers=True,
                          drop_last=True)

    def val_dataloader(self) -> EVAL_DATALOADERS:
        val_list = str(self.annotation_path / self.hparams.val_list)
        parser = self.parser_class(root=self.data_path, img_list=val_list, split="validation", emo_path=self.pseudo_label_path / self.hparams.dataset)
        val_dataset = MTLDataset(self.data_path, parser,
                                 transform=[self.eval_transforms, self.eval_transforms_2],
                                 input_sizes=[self.hparams.input_size_1, self.hparams.input_size_2])
        val_sampler = SequentialSampler(val_dataset)
        return DataLoader(val_dataset,
                          sampler=val_sampler,
                          batch_size=int(1.5 * self.hparams.batch_size),
                          num_workers=self.hparams.num_workers,
                          pin_memory=True,
                          persistent_workers=True,
                          drop_last=False)

    def test_dataloader(self) -> EVAL_DATALOADERS:
        return self.val_dataloader()

    def predict_dataloader(self) -> EVAL_DATALOADERS:
        pred_list = str(self.annotation_path / self.hparams.pred_list)
        parser = self.parser_class(root=self.data_path, img_list=pred_list, split="test", emo_path=self.pseudo_label_path / self.hparams.dataset)
        predict_dataset = MTLDataset(self.data_path, parser,
                                     transform=[self.eval_transforms, self.eval_transforms_2],
                                     input_sizes=[self.hparams.input_size_1, self.hparams.input_size_2])
        val_sampler = SequentialSampler(predict_dataset)
        return DataLoader(predict_dataset,
                          sampler=val_sampler,
                          batch_size=1,
                          num_workers=self.hparams.num_workers,
                          pin_memory=True,
                          persistent_workers=True,
                          drop_last=False)


class MTLDataset(ImageDataset):
    NUM_IMG_CHANNELS = 3
    NON_FACE_LABEL = 7

    def __init__(self, root, parser, transform, input_sizes):
        super(MTLDataset, self).__init__(root, parser=parser, transform=transform)
        self.t1, self.t2 = self.transform
        self.input_sizes = input_sizes

    def __getitem__(self, index):
        img_1, target1, img_2, target2 = self.parser[index]
        img_1 = Image.open(img_1).convert('RGB')
        img_1 = self.t1(img_1)
        if img_2 is not None:
            img_2 = Image.open(img_2).convert('RGB')
            img_2 = self.t2(img_2)
            return img_1, target1, img_2, target2
        else:
            img_2 = torch.zeros([MTLDataset.NUM_IMG_CHANNELS, self.input_sizes[1], self.input_sizes[1]])
            return img_1, target1, img_2, MTLDataset.NON_FACE_LABEL


if __name__ == "__main__":
    checkpoint_callback = ModelCheckpoint(
        monitor = "Accuracy/val_driver",
        save_top_k = 1,
        dirpath="/data/shared/ACVPR Group 1 2024/",
        filename="vit-aide-maarten-{epoch:02d}",
    )

    checkpoint_callback_mtl = ModelCheckpoint(
        monitor = "Accuracy/val_driver",
        save_top_k = 1,
        dirpath="/data/shared/ACVPR Group 1 2024/",
        filename="vit-dd-aide-maarten-{epoch:02d}",
    )


    # Create Data Modules
    dm_mtl = MTLDistractedDriverLDM(dataset="AIDE", batch_size=4, num_workers=2)
    # dm = DistractedDriverLDM(dataset="AIDE", batch_size=4, num_workers=2)

    # Create model
    model_mtl = ViTDDLM(num_classes_1=7, num_classes_2=5, model="vit_base_patch16_224")
    # model = VisionTransformerLM(num_classes=7, model="vit_base_patch16_224")

    # Train model
    # logger = TensorBoardLogger(save_dir=os.getcwd(), name="lightning_logs")
    # trainer = Trainer(max_epochs=5, callbacks=[checkpoint_callback], logger=logger, accelerator='gpu', devices=[0,1])
    # trainer.fit(model, dm.train_dataloader(), dm.val_dataloader())

    # Train MTL model
    logger_mtl = TensorBoardLogger(save_dir=os.getcwd(), name="lightning_logs")
    trainer_mtl = Trainer(max_epochs=5, callbacks=[checkpoint_callback_mtl], logger=logger_mtl, accelerator='gpu', devices=[0,1])
    trainer_mtl.fit(model_mtl, dm_mtl.train_dataloader(), dm_mtl.val_dataloader())
