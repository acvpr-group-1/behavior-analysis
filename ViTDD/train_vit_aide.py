from datasets import load_dataset
import torch
from torch.utils.data import DataLoader
from pytorch_lightning import LightningDataModule
from transformers import ViTImageProcessor
from pytorch_lightning import Trainer
from train import ViTDDLM, VisionTransformerLM


DATA_DIR = "/data/shared/ACVPR Group 1 2024/AIDE_splits"
MODEL_DIR = "/data/shared/ACVPR Group 1 2024/vit-aide-10e"
CHECKPOINT_FILE = "/data/shared/ACVPR Group 1 2024/vit-aide-10e.ckpt"


class AIDEDataModule(LightningDataModule):
    def __init__(self, dataset_name: str, batch_size: int):
        super().__init__()
        self.dataset_name = dataset_name
        self.batch_size = batch_size
        self.tokenizer = ViTImageProcessor.from_pretrained('google/vit-base-patch16-224-in21k')

    def prepare_data(self):
        # Download data and prepare data
        self.dataset = load_dataset("imagefolder", data_dir=self.dataset_name)
        self.dataset = self.dataset.with_transform(transform=self.transform)

    def transform(self, sample):
        inputs = self.tokenizer([x for x in sample['image']], return_tensors='pt')
        inputs['label'] = sample['label']
        return inputs

    def setup(self, stage=None):
        # Split dataset
        self.train_dataset = self.dataset['train']
        self.val_dataset = self.dataset['validation']
        self.test_dataset = self.dataset['test']

    def collate_fn(self, batch):
        return [
            torch.stack([x['pixel_values'] for x in batch]),
            torch.tensor([x['label'] for x in batch])
        ]

    def train_dataloader(self):
        return DataLoader(self.train_dataset, batch_size=self.batch_size, collate_fn=self.collate_fn)

    def val_dataloader(self):
        return DataLoader(self.val_dataset, batch_size=self.batch_size, collate_fn=self.collate_fn)

    def test_dataloader(self):
        return DataLoader(self.test_dataset, batch_size=self.batch_size, collate_fn=self.collate_fn)


dm = AIDEDataModule(dataset_name=DATA_DIR, batch_size=4)
dm.prepare_data()
dm.setup()

# Create model
model = VisionTransformerLM(num_classes=7)

# Create trainer
trainer = Trainer(max_epochs=10)

# Train model
trainer.fit(model, dm.train_dataloader())

# Save checkpoint
trainer.save_checkpoint(CHECKPOINT_FILE)