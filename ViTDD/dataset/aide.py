from pytorch_lightning import LightningDataModule
from transformers import ViTImageProcessor
from datasets import load_dataset
from torch.utils.data import DataLoader
import torch


class AIDEDataModule(LightningDataModule):
    def __init__(self, dataset_name: str, batch_size: int):
        super().__init__()
        self.dataset_name = dataset_name
        self.batch_size = batch_size
        self.tokenizer = ViTImageProcessor.from_pretrained('google/vit-base-patch16-224')
        self.prepare_data()
        self.setup()

    def prepare_data(self):
        # Download data and prepare data
        self.dataset = load_dataset("imagefolder", data_dir=self.dataset_name)
        self.dataset = self.dataset.with_transform(transform=self.transform)

    def transform(self, sample):
        inputs = self.tokenizer([x for x in sample['image']], return_tensors='pt')
        inputs['label'] = sample['label']
        return inputs

    def setup(self):
        # Split dataset
        self.train_dataset = self.dataset['train']
        self.val_dataset = self.dataset['validation']
        self.test_dataset = self.dataset['test']

    def collate_fn(self, batch):
        return [
            torch.stack([x['pixel_values'] for x in batch]),
            torch.tensor([x['label'] for x in batch])
        ]

    def train_dataloader(self):
        return DataLoader(self.train_dataset, batch_size=self.batch_size, collate_fn=self.collate_fn, shuffle=True)

    def val_dataloader(self):
        return DataLoader(self.val_dataset, batch_size=self.batch_size, collate_fn=self.collate_fn, shuffle=True)

    def test_dataloader(self):
        return DataLoader(self.test_dataset, batch_size=self.batch_size, collate_fn=self.collate_fn, shuffle=True)
