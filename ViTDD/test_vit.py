import torch
from torch.utils.data import DataLoader, RandomSampler, SequentialSampler
from pytorch_lightning import LightningDataModule
from pytorch_lightning import Trainer
from train import ViTDDLM, VisionTransformerLM
from timm.data.parsers.parser import Parser
from pathlib import Path
from timm.data import ImageDataset
from pytorch_lightning.utilities.types import TRAIN_DATALOADERS, EVAL_DATALOADERS
from torchvision.transforms import InterpolationMode, GaussianBlur, RandomSolarize, RandomGrayscale
from torchvision import transforms
from timm.data.constants import IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD
import csv
import os
from PIL import Image
from sklearn.metrics import classification_report, confusion_matrix, ConfusionMatrixDisplay
import matplotlib.pyplot as plt
from tqdm import tqdm


from train import ViTDDLM, VisionTransformerLM
from train_vit_dd_aide import MTLDistractedDriverLDM, DistractedDriverLDM


DATA_DIR = "/data/shared/ACVPR Group 1 2024/AIDE"

vit_model = VisionTransformerLM.load_from_checkpoint(checkpoint_path="/data/shared/ACVPR Group 1 2024/vit-cato-1e-no-aug.ckpt")
vit_dd_model = ViTDDLM.load_from_checkpoint(checkpoint_path="/data/shared/ACVPR Group 1 2024/vit-dd-cato-1e-ni-aug.ckpt")
dm = DistractedDriverLDM(dataset="AIDE", batch_size=4, num_workers=2)
dm_dd = MTLDistractedDriverLDM(dataset="AIDE", batch_size=4, num_workers=2)


# Specify the device to use
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
# device = torch.device('cpu')


def test_model(model, name, is_dd=False):
    model = model.to(device)

    behavior_predictions = []
    behavior_actual = []

    emotion_predictions = []
    emotion_actual = []

    with torch.no_grad():
        if is_dd:
            dataloader = dm_dd.predict_dataloader()
        else:
            dataloader = dm.predict_dataloader()
        for data in tqdm(dataloader):
            if is_dd:
                images, labels, face_images, emotion_labels = data
                face_images = face_images.to(device)
                emotion_labels = emotion_labels.to(device)
            else:
                images, labels = data

            # Move the input data to the device
            images = images.to(device)
            labels = labels.to(device)
            
            if is_dd:
                outputs = model(images, face_images)
                
                # Get predictions
                _, behavior_predicted = torch.max(outputs[0].data, 1)
                _, emotion_predicted = torch.max(outputs[1].data, 1)
                
                emotion_predictions.extend(emotion_predicted.cpu().numpy())
                emotion_actual.extend(emotion_labels.cpu().numpy())
            else:    
                outputs = model(images)

                # Get predictions
                _, behavior_predicted = torch.max(outputs.data, 1)

            # Save predictions
            behavior_predictions.extend(behavior_predicted.cpu().numpy())
            behavior_actual.extend(labels.cpu().numpy())


    print(classification_report(behavior_actual, behavior_predictions))
    cm = confusion_matrix(behavior_actual, behavior_predictions, normalize='pred')
    disp = ConfusionMatrixDisplay(confusion_matrix=cm)
    disp.plot(xticks_rotation="vertical")
    plt.show()
    plt.savefig(f'{name}behavior.png', bbox_inches='tight')
    
    if is_dd:
        print(classification_report(emotion_actual, emotion_predictions))
        cm = confusion_matrix(emotion_actual, emotion_predictions, normalize='pred')
        disp = ConfusionMatrixDisplay(confusion_matrix=cm)
        disp.plot(xticks_rotation="vertical")
        plt.show()
        plt.savefig(f'{name}emotion.png', bbox_inches='tight')
        
        
test_model(vit_model, "vit-cato-1e-no-aug")
test_model(vit_dd_model, "vit-dd-cato-1e-no-aug", True)
