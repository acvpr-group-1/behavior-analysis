import os
import json
import cv2
import numpy as np
import pandas as pd
import torch
from torch.utils.data import DataLoader, Dataset
from PIL import Image

EMOTION_LABEL = ['Anxiety', 'Peace', 'Weariness', 'Happiness', 'Anger']
DRIVER_BEHAVIOR_LABEL = ['Smoking', 'Making Phone', 'Looking Around', 'Dozing Off', 'Normal Driving', 'Talking',
                         'Body Movement']

class CarDataset(Dataset):

    def __init__(self, csv_file):

        self.path = pd.read_csv(csv_file)
        self.resize_height = 224
        self.resize_width = 224

    def __len__(self):
        return len(self.path)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        frames_path, label_path = self.path.iloc[idx]
        label_json = json.load(open(label_path))
        buffer = self.load_frames(frames_path)
        context = self.randomflip(buffer)
        context = self.to_tensor(context)

        emotion_label = EMOTION_LABEL.index((label_json['emotion_label'].capitalize()))
        driver_behavior_label = DRIVER_BEHAVIOR_LABEL.index((label_json['driver_behavior_label']))

        sample = {
            'context': context,
            "emotion_label": emotion_label,
            "driver_behavior_label": driver_behavior_label,
        }

        context = sample['context']
        emotion_label = sample['emotion_label']
        behavior_label = sample['driver_behavior_label']

        return context, emotion_label, behavior_label

    def load_frames(self, file_dir):

        incar_path = os.path.join(file_dir, 'incarframes')

        frames = [os.path.join(incar_path, img) for img in os.listdir(incar_path) if img.endswith('.jpg')]

        frames.sort(key=lambda x: int(os.path.basename(x).split('.')[0]))

        buffer = []

        for i, frame_name in enumerate(frames):
            if not i == 0 and not i % 3 == 2:
                continue
            if i >= 45:
                break

            img = cv2.imread(frame_name)

            if img.shape[0] != self.resize_height or img.shape[1] != self.resize_width:
                img = cv2.resize(img, (self.resize_width, self.resize_height))

            buffer.append(torch.from_numpy(img).float())

        return torch.stack(buffer)

    def randomflip(self, buffer):

        if np.random.random() < 0.5:
            buffer = torch.flip(buffer, dims=[1])

        if np.random.random() < 0.5:
            buffer = torch.flip(buffer, dims=[2])

        return buffer

    def normalize(self, buffer):
        for i, frame in enumerate(buffer):
            frame -= np.array([[[90.0, 98.0, 102.0]]])
            buffer[i] = frame

        return buffer

    def to_tensor(self, buffer):

        return buffer.permute(3, 0, 1, 2).contiguous()


class OurDataset(Dataset):

    def __init__(self, csv_file):
        self.path = pd.read_csv(csv_file)

    def __len__(self):
        return len(self.path) * 45 # Number of frames per video

    def __getitem__(self, idx):
        video_idx = idx // 45
        frame_idx = idx % 45

        frames_path, label_path = self.path.iloc[video_idx]
        label_json = json.load(open(label_path))

        image = Image.open(frames_path + "incarframes/" + str(frame_idx) + ".jpg")

        emotion_label = EMOTION_LABEL.index((label_json['emotion_label'].capitalize()))
        driver_behavior_label = DRIVER_BEHAVIOR_LABEL.index((label_json['driver_behavior_label']))

        return image, emotion_label, driver_behavior_label


if __name__ == "__main__":
    train_dataset = CarDataset(csv_file='training_new.csv')
    train_dataloader = DataLoader(train_dataset, batch_size=1, shuffle=True, num_workers=16, drop_last=False)

    for i_batch, sample in enumerate(train_dataloader):
        context, emotion_label, behavior_label = sample
        print(
            'context:{}, emotion_label:{}, behavior_label:{}' \
            .format(context.shape, emotion_label.shape, behavior_label.shape))
