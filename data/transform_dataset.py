import os
import shutil
import json
import pandas as pd
import tqdm
from pathlib import Path
from PIL import Image
import numpy as np

EMOTION_LABEL = ['anxiety', 'peace', 'weariness', 'happiness', 'anger']

def transform(new_data_path, new_emotion_path, new_annotation_path):
    train_csv = pd.read_csv("training.csv")
    test_csv = pd.read_csv("testing.csv")
    validation_csv = pd.read_csv("validation.csv")

    train_csv['split'] = 'train'
    test_csv['split'] = 'test'
    validation_csv['split'] = 'validation'

    new_data_path = Path(new_data_path)
    new_emotion_path = Path(new_emotion_path)
    new_annotation_path = Path(new_annotation_path)
    videos = pd.concat([train_csv, test_csv, validation_csv])

    os.makedirs(new_annotation_path, exist_ok=True)
    with open(new_annotation_path / "train_list_01.csv", "w") as a: # Clear file if exists
        a.write("Subject, Class, Image\n")
    with open(new_annotation_path / "test_list_01.csv", "w") as a: # Clear file if exists
        a.write("Subject, Class, Image\n")
    with open(new_annotation_path / "validation_list_01.csv", "w") as a: # Clear file if exists
        a.write("Subject, Class, Image\n")

    os.makedirs(new_emotion_path / "imgs", exist_ok=True)
    with open(new_emotion_path / "emo_list.csv", "w") as e: # Clear file if exists
        e.write("")

    new_image_id = 0
    for index, row in tqdm.tqdm(videos.iterrows(), total=videos.shape[0]):
        video_path, label_path, split = row
        video_path = Path(video_path)
        with open(label_path) as l:
            label_json = json.load(l)
        driver_behaviour_label = label_json['driver_behavior_label']
        emotion_label = label_json['emotion_label']

        for frame_id in range(45): # Every video as 45 frames
            frame_image = Image.open(video_path / "incarframes" / f"{frame_id}.jpg")

            detected_people = label_json['pose_list'][frame_id]['result']
            if len(detected_people) == 0:
                print("Found zero people, skipping. File: ", video_path / "incarframes" / f"{frame_id}.jpg")
                new_image_id += 1
                continue
            elif len(detected_people) > 1:
                proposal_scores = [r['proposal_score'] for r in detected_people]
                face_bbox = detected_people[np.argmax(proposal_scores)]['face_bbox']
            else:
                face_bbox = detected_people[0]['face_bbox']

            if face_bbox[2] < 20 or face_bbox[3] < 20:
                print("Bounding box was very small, skipping. File: ", video_path / "incarframes" / f"{frame_id}.jpg")
                continue

            face_image = frame_image.crop((face_bbox[0], face_bbox[1], face_bbox[0]+face_bbox[2], face_bbox[1]+face_bbox[3]))

            with open(new_annotation_path / f"{split}_list_01.csv", "a") as a:
                a.write(f",{driver_behaviour_label},{new_image_id}.jpg\n")

            with open(new_emotion_path / "emo_list.csv", "a") as e:
                e.write(f"./{new_image_id}.jpg, {EMOTION_LABEL.index(emotion_label.lower())}\n")

            face_image.save(new_emotion_path / 'imgs' / f"{new_image_id}_face.jpg")
            
            new_data_location = new_data_path / split / driver_behaviour_label
            os.makedirs(new_data_location, exist_ok=True)
            frame_image.save(new_data_location / f"{new_image_id}.jpg")

            new_image_id += 1


if __name__ == "__main__":
    os.makedirs("/data/shared/ACVPR Group 1 2024/AIDE", exist_ok=True)
    transform("/data/shared/ACVPR Group 1 2024/AIDE/dataset", "/data/shared/ACVPR Group 1 2024/AIDE/emotions", "/data/shared/ACVPR Group 1 2024/AIDE/annotations")