import pandas as pd
import os
import shutil

AIDE_PATH = "/data/shared/ACVPR Group 1 2024/AIDE_Filtered"


def replace_path(csv_file, new_csv_file):
    # Read the CSV file
    df = pd.read_csv(csv_file)

    # Replace all instances of "AIDE_Dataset" with the new variable in each cell
    df = df.map(lambda x: x.replace('AIDE_Dataset', AIDE_PATH) if isinstance(x, str) else x)

    # Write the data back to the CSV file
    df.to_csv(new_csv_file, index=False)


def filter_dataset():
    print("Copying folder annotation")
    shutil.copytree(os.path.join(AIDE_PATH, 'annotation'), os.path.join(AIDE_PATH, 'annotation'))

    for i in range(3063):
        source_dir = os.path.join(AIDE_PATH, "{:04d}".format(i))
        destination_dir = os.path.join(AIDE_PATH, "{:04d}".format(i))

        if os.path.exists(source_dir):
            print("Copying folder {:04d}".format(i))

            if not os.path.exists(destination_dir):
                os.makedirs(destination_dir)

            shutil.copytree(os.path.join(source_dir, 'incarframes'), os.path.join(destination_dir, 'incarframes'))
            shutil.copy2(os.path.join(source_dir, 'incar.mp4'), destination_dir)


if __name__ == "__main__":
    replace_path("training_old.csv", "training.csv")
    replace_path("validation_old.csv", "validation.csv")
    replace_path("testing_old.csv", "testing.csv")
    # filter_dataset()
